import React, { useState } from "react";
import { View } from "react-native";
import { TextInput, Button } from "react-native-paper";
import { useFormik } from "formik";
import * as Yup from "yup";
import Toast from "react-native-root-toast";
import { registerApi } from "../../api/user";
import { Icon } from "react-native-elements";
import { formStyle } from "../../styles";
import email from "react-native-email";
import { Linking } from "react-native";

export default function RegisterForm(props) {
  const { changeForm } = props;

  const [loading, setLoading] = useState(false);

  const [secure, setSecure] = useState(true);

  const [secureR, setSecureR] = useState(true);

  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      setLoading(true);
      try {
        await registerApi(formData);
        changeForm();
      } catch (error) {
        setLoading(false);
        Toast.show("Error al registrar el usuario", {
          position: Toast.positions.CENTER,
        });
      }
    },
  });

  /*const handleEmail = () => {
        const to = ['henryxhdezcruz@gmail.com', '20181136@uthh.edu.mx'] // string or array of email addresses
        email(to, {
            // Optional additional arguments
            cc: ['bazzy@moo.com', 'doooo@daaa.com'], // string or array of email addresses
            bcc: 'mee@mee.com', // string or array of email addresses
            subject: 'Recuperar contraseña',
            body: 'Código de verificación'
        }).catch(console.error)
    }*/

  /*sendEmail = (to, subject, body) => {
        Linking.openURL(`mailto:${to}?subject=${subject}&body=${body}`);
    }*/

  /*const sendEmail = () => {
        Linking.openURL(`mailto:henryxhdezcruz@gmail.com?subject="Recuperar contraseña"&body="Código de verificación"`);
    }*/

  return (
    <View>
      <TextInput
        label="Email"
        maxLength={50}
        style={formStyle.input}
        onChangeText={(text) => formik.setFieldValue("email", text)}
        value={formik.values.email}
        error={formik.errors.email}
      />

      <TextInput
        label="Nombre de usuario"
        maxLength={40}
        style={formStyle.input}
        onChangeText={(text) => formik.setFieldValue("username", text)}
        value={formik.values.username}
        error={formik.errors.username}
      />

      <View style={{ flexDirection: "row", width: "100%", marginBottom: 20 }}>
        <TextInput
          label="Contraseña"
          maxLength={40}
          style={([formStyle.input], { width: "100%" })}
          onChangeText={(text) => formik.setFieldValue("password", text)}
          value={formik.values.password}
          error={formik.errors.password}
          secureTextEntry={secureR}
        />
        <View
          style={{
            width: "100%",
            position: "absolute",
            alignSelf: "center",
            alignItems: "flex-end",
            paddingRight: 20,
          }}
        >
          <Icon
            type="font-awesome"
            style={{ position: "absolute", marginRight: 50 }}
            name={secureR ? "eye" : "eye-slash"}
            color="gray"
            onPress={() => setSecureR(!secureR)}
          />
        </View>
      </View>

      <View style={{ flexDirection: "row", width: "100%", marginBottom: 20 }}>
        <TextInput
          label="Confirmar contraseña"
          maxLength={40}
          style={([formStyle.input], { width: "100%" })}
          onChangeText={(text) => formik.setFieldValue("repeatPassword", text)}
          value={formik.values.repeatPassword}
          error={formik.errors.repeatPassword}
          secureTextEntry={secure}
        />
        <View
          style={{
            width: "100%",
            position: "absolute",
            alignSelf: "center",
            alignItems: "flex-end",
            paddingRight: 20,
          }}
        >
          <Icon
            type="font-awesome"
            style={{ position: "absolute", marginRight: 50 }}
            name={secure ? "eye" : "eye-slash"}
            color="gray"
            onPress={() => setSecure(!secure)}
          />
        </View>
      </View>

      <Button
        mode="contained"
        style={formStyle.btnSuccess}
        onPress={formik.handleSubmit}
        loading={loading}
      >
        Registrarse
      </Button>

      <Button
        mode="text"
        style={formStyle.btnText}
        labelStyle={formStyle.btnTextLabel}
        onPress={changeForm}
      >
          ¿Ya tienes una cuenta?
      </Button>
    </View>
  );
}

function initialValues() {
  return {
    email: "",
    username: "",
    password: "",
    repeatPassword: "",
  };
}

function validationSchema() {
  return {
    email: Yup.string().email(true).required(true),
    username: Yup.string().required(true),
    password: Yup.string().required(true),
    repeatPassword: Yup.string()
      .required(true)
      .oneOf([Yup.ref("password")], true),
  };
}
