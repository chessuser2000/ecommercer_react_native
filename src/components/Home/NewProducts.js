import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { getLastProductApi } from "../../api/product";
import ListProduct from "./ListProduct";

export default function NewProducts() {
  const [products, setProducts] = useState(null);

  useEffect(() => {
    (async () => {
      const response = await getLastProductApi();
      setProducts(response);
    })();
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Nuevos productos</Text>
      {products && (
        <TouchableOpacity>
          <ListProduct products={products} />
        </TouchableOpacity>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    marginTop: 20,
    paddingBottom: 120,
  },
  title: {
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 10,
  },
});
