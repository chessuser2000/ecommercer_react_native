import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from "react-native-paper";
//import Toast from 'react-native-root-toast';
//import Toast  from  'react-native-toast-message' ;
import { useToast } from "react-native-fast-toast";
import { addProductCartApi } from "../../api/cart";
import { Icon } from "react-native-elements";

/*<Button
mode="contained"
contentStyle={styles.btnBuyContent}
labelStyle={styles.btnLabel}
style={styles.btn}
onPress={addProductCart}
>
Añadir al carrito
</Button>*/

export default function Buy(props) {
  const { product, quantity } = props;

  const toast = useToast();

  const addProductCart = async () => {
    const response = await addProductCartApi(product._id, quantity);
    if (response) {
      //toast.show("Producto añadido al carrito")
      toast.show("Producto añadido al carrito", {
        //type: 'success',
        position: "top",
        duration: 4000,
        offset: 40,
        animationType: "zoom-in",
      });
    } else {
      toast.show("ERROR al añadir el producto al carrito", {
        position: "top",
        duration: 4000,
        offset: 40,
        animationType: "zoom-in",
      });
    }
  };

  return (
    <View style={{ zIndex: 1 }}>
      <Icon
        name="shopping-cart"
        type="font-awesome"
        color="#15803D"
        //reverse
        size={25}
        raised
        onPress={addProductCart}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  btnBuyContent: {
    backgroundColor: "#15803D",
    paddingVertical: 5,
  },
  btnLabel: {
    fontSize: 18,
  },
  btn: {
    marginTop: 20,
  },
});
