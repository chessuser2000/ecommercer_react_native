const colors = {
  primary: "#15803D",
  secondary: "#0891B2",
  dark: "#164E63",
  //fonts
  fontLight: "#ECFEFF",
  //background
  bgLight: "#ECFEFF",
  bgDark: "#22C55E",
};

export default colors;
