import React from "react";
import { StyleSheet, ScrollView, Button } from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import Search from "../../components/Search";
import NewProducts from "../../components/Home/NewProducts";
import ScreenLoading from "../../components/ScreenLoading";
import StatusBar from "../../components/StatusBar";
import Banner from "../../components/Home/Banner";
import colors from "../../styles/colors";
import { RefreshControl } from "react-native";
//const navigation = useNavigation();

export default function Home() {
  const wait = (timeout) => {
    return new Promise((resolve) => setTimeout(resolve, timeout));
  };
  const [refreshing, setRefreshing] = React.useState(false);
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, []);
  return (
    <>
      <StatusBar backgroundColor={colors.bgDark} barStyle="light-content" />
      <Search />
      <ScrollView
        style={styles.container}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <Banner />
        <NewProducts />
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingBottom: 100,
  },
});
