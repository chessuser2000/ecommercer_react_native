import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text, ScrollView } from "react-native";
import StatusBar from "../../components/StatusBar";
import Search from "../../components/Search";
import ScreenLoading from "../../components/ScreenLoading";
import { useFormik } from "formik";
import * as Yup from "yup";
import Toast from "react-native-root-toast";
import { getProductApi } from "../../api/product";
import { searchProductApi } from "../../api/search";
import colors from "../../styles/colors";
import CarouselImage from "../../components/Product/CarouselImage";
import Price from "../../components/Product/Price";
import Quantity from "../../components/Product/Quantity";
import Buy from "../../components/Product/Buy";
import Favorite from "../../components/Product/Favorite";
import RelatedProducts from "../../components/Product/RelatedProducts";
import splitOnFirst from "split-on-first";
import { map } from "lodash";
import { Button } from "react-native-paper";
const split = require("split-string");

export default function Product(props) {
  const { route } = props;

  const { params } = route;

  const [product, setProduct] = useState(null);

  const [images, setImages] = useState([]);

  const [quantity, setQuantity] = useState(1);

  const [arrTags, setArrTags] = useState([]);

  const [relatedProducts, setRelatedProducts] = useState([]);

  useEffect(() => {
    setProduct(null);
    (async () => {
      const response = await getProductApi(params.idProduct);
      setProduct(response);

      const arrayImages = [response.main_image];
      arrayImages.push(...response.images);
      setImages(arrayImages);
      const arr = split(response.tags);

      console.log(arr.length);
      for (let index = 0; index < arr.length; index++) {
        //const element = array[index];
        const res = await searchProductApi(arr[index]);
        setArrTags(arrTags.concat(res));
        //console.log(arrT + arr[index] + index );
        //console.log(arrT);
        setRelatedProducts(arrT);
      }

      console.log(relatedProducts);

      // map(arrTags, async (arrTag) => {

      //     //setRelatedProducts(response);
      // });
    })();
  }, [params]);

  return (
    <>
      <StatusBar backgroundColor={colors.bgDark} barStyle={"light-content"} />
      <Search />
      {!product ? (
        <ScreenLoading text="Cargando..." size="large" />
      ) : (
        <ScrollView style={styles.container}>
          <Text style={styles.title}>{product.title}</Text>
          <CarouselImage images={images} />
          <View style={styles.containerView}>
            <Price price={product.price} discount={product.discount} />
            <Quantity quantity={quantity} setQuantity={setQuantity} />
            <View
              style={{
                paddingTop: 20,
                //marginLeft: 20,
                flexDirection: "row",
                justifyContent: "space-evenly",
              }}
            >
              <Favorite product={product} />
              <Buy product={product} quantity={quantity} />
            </View>
            {/* <RelatedProducts /> */}
            <ScrollView horizontal></ScrollView>
          </View>
        </ScrollView>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingBottom: 50,
  },
  title: {
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 20,
    padding: 10,
  },
  containerView: {
    padding: 10,
    paddingBottom: 200,
  },
});
