import React, { useState, useCallback } from "react";
import { ScrollView, StyleSheet } from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import Search from "../../components/Search";
import ScreenLoading from "../../components/ScreenLoading";
import StatusBar from "../../components/StatusBar";
import colors from "../../styles/colors";
import { getMeApi } from "../../api/user";
import useAuth from "../../hooks/useAuth";
import UserInfo from "../../components/Account/UserInfo";
import Menu from "../../components/Account/Menu";
import { RefreshControl } from "react-native";
export default function Account() {
  const [user, setUser] = useState(null);
  const { auth } = useAuth();
  console.log(auth);

  useFocusEffect(
    useCallback(() => {
      (async () => {
        const response = await getMeApi(auth.token);
        setUser(response);
      })();
    }, [])
  );
  const wait = (timeout) => {
    return new Promise((resolve) => setTimeout(resolve, timeout));
  };
  const [refreshing, setRefreshing] = React.useState(false);
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  return (
    <>
      <StatusBar backgroundColor={colors.bgDark} barStyle="light-content" />

      {!user ? (
        <ScreenLoading size="large" text="Cargando..." color="#0891B2" />
      ) : (
        <>
          <Search />
          <ScrollView
            style={styles.container}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
          >
            <UserInfo user={user} />
            <Menu />
          </ScrollView>
        </>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 100,
  },
});
